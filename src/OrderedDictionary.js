// OrderedDictionary<Key, Value> ::= { all :: { Key -> Value }, order: [Key] }

export const empty = { all: {}, order: [] };

// fromArray :: [{ key :: Key, value :: Value }] -> OrderedDictionary<Key, Value>
export const fromArray =
	array => array.reduce((dict, kvPair, index) => insert({ ...kvPair, index })(dict), empty);

export const insert = ({ key, value, index }) => dictionary => ({
	all: { ...dictionary.all, [key]: value },
	order: index === -1 
		? [...dictionary.order, key]
		: [...dictionary.order.slice(0, index), key, ...dictionary.order.slice(index)]
});

// toValuesList :: OrderedDictionary<Key, Value> -> [Value]
export const toValuesList = ({ all, order }) => order.map(key => all[key]);

// toList :: OrderedDictionary<Key, Value> -> [{ key: Key, value: Value }]
export const toList = ({ all, order }) => order.map(key => ({ key, value: all[key] }));

// toUnorderedObject :: OrderedDictionary<Key, Value> -> { Key -> Value }
export const toUnorderedObject = ({ all }) => all;

export const contains = key => dict => key in dict.all;

export const set = key => value => dict => {
	if (!contains(key)(dict)) {
		return dict;
	}

	return {
		...dict,
		all: {
			...dict.all,
			[key]: value
		}, 
	};
};


export const push = ({ key, value }) => insert({ key, value, index: -1 });

// get :: Key -> OrderedDictionary<Key, Value> -> Value?
export const get = key => dict => dict.all[key];

// update :: Key -> (Value -> Value) -> OrderedDictionary<Key, Value> -> OrderedDictionary<Key, Value>
export const update = key => updater => dict => {
	if (!contains(key)(dict)) {
		return dict;
	}

	return set(key)(updater(get(key)(dict)))(dict);
};

// mapValues :: (Value -> Value') -> OrderedDictionary<Key, Value> -> OrderedDictionary<Key, Value'>
export const mapValues = transform => dict =>
	dict.order.reduce((acc, key) => update(key)(transform)(acc), dict);

// filter :: ([Key, Value] -> Bool) -> OrderedDictionary<Key, Value> -> OrderedDictionary<Key, Value>
export const filter = predicate => dict =>
	dict.order
		.map((key, index) => ({ key, value: get(key)(dict) }))
		.reduce((acc, entry) =>
			predicate([entry.key, entry.value]) 
			? push(entry)(acc)
			: acc, empty);
	
