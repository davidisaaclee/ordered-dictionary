'use strict';

var _assert = require('assert');

var _assert2 = _interopRequireDefault(_assert);

var _dist = require('../dist');

var OrderedDictionary = _interopRequireWildcard(_dist);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe("Ordered dictionary", function () {
	it("should be able to construct empty dictionary", function () {
		_assert2.default.deepEqual(OrderedDictionary.empty, { all: {}, order: [] });
	});
});