'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _OrderedDictionary = require('./OrderedDictionary');

Object.keys(_OrderedDictionary).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _OrderedDictionary[key];
    }
  });
});