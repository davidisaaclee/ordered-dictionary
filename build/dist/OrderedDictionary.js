"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// OrderedDictionary<Key, Value> ::= { all :: { Key -> Value }, order: [Key] }

var empty = exports.empty = { all: {}, order: [] };

// fromArray :: [{ key :: Key, value :: Value }] -> OrderedDictionary<Key, Value>
var fromArray = exports.fromArray = function fromArray(array) {
	return array.reduce(function (dict, kvPair, index) {
		return insert(_extends({}, kvPair, { index: index }))(dict);
	}, empty);
};

var insert = exports.insert = function insert(_ref) {
	var key = _ref.key,
	    value = _ref.value,
	    index = _ref.index;
	return function (dictionary) {
		return {
			all: _extends({}, dictionary.all, _defineProperty({}, key, value)),
			order: index === -1 ? [].concat(_toConsumableArray(dictionary.order), [key]) : [].concat(_toConsumableArray(dictionary.order.slice(0, index)), [key], _toConsumableArray(dictionary.order.slice(index)))
		};
	};
};

// toValuesList :: OrderedDictionary<Key, Value> -> [Value]
var toValuesList = exports.toValuesList = function toValuesList(_ref2) {
	var all = _ref2.all,
	    order = _ref2.order;
	return order.map(function (key) {
		return all[key];
	});
};

// toList :: OrderedDictionary<Key, Value> -> [{ key: Key, value: Value }]
var toList = exports.toList = function toList(_ref3) {
	var all = _ref3.all,
	    order = _ref3.order;
	return order.map(function (key) {
		return { key: key, value: all[key] };
	});
};

// toUnorderedObject :: OrderedDictionary<Key, Value> -> { Key -> Value }
var toUnorderedObject = exports.toUnorderedObject = function toUnorderedObject(_ref4) {
	var all = _ref4.all;
	return all;
};

var contains = exports.contains = function contains(key) {
	return function (dict) {
		return key in dict.all;
	};
};

var set = exports.set = function set(key) {
	return function (value) {
		return function (dict) {
			if (!contains(key)(dict)) {
				return dict;
			}

			return _extends({}, dict, {
				all: _extends({}, dict.all, _defineProperty({}, key, value))
			});
		};
	};
};

var push = exports.push = function push(_ref5) {
	var key = _ref5.key,
	    value = _ref5.value;
	return insert({ key: key, value: value, index: -1 });
};

// get :: Key -> OrderedDictionary<Key, Value> -> Value?
var get = exports.get = function get(key) {
	return function (dict) {
		return dict.all[key];
	};
};

// update :: Key -> (Value -> Value) -> OrderedDictionary<Key, Value> -> OrderedDictionary<Key, Value>
var update = exports.update = function update(key) {
	return function (updater) {
		return function (dict) {
			if (!contains(key)(dict)) {
				return dict;
			}

			return set(key)(updater(get(key)(dict)))(dict);
		};
	};
};

// mapValues :: (Value -> Value') -> OrderedDictionary<Key, Value> -> OrderedDictionary<Key, Value'>
var mapValues = exports.mapValues = function mapValues(transform) {
	return function (dict) {
		return dict.order.reduce(function (acc, key) {
			return update(key)(transform)(acc);
		}, dict);
	};
};

// filter :: ([Key, Value] -> Bool) -> OrderedDictionary<Key, Value> -> OrderedDictionary<Key, Value>
var filter = exports.filter = function filter(predicate) {
	return function (dict) {
		return dict.order.map(function (key, index) {
			return { key: key, value: get(key)(dict) };
		}).reduce(function (acc, entry) {
			return predicate([entry.key, entry.value]) ? push(entry)(acc) : acc;
		}, empty);
	};
};