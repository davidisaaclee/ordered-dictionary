import assert from 'assert';
import * as OrderedDictionary from '../dist';

describe("Ordered dictionary", () => {
	it("should be able to construct empty dictionary", () => {
		assert.deepEqual(OrderedDictionary.empty, { all: {}, order: [] });
	});
});

